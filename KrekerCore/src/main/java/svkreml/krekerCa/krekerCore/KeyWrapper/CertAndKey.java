package svkreml.krekerCa.krekerCore.KeyWrapper;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;

/*
 * вспомогательный класс чтобы можно было взвращать сертификаты и закрытый ключ одним объектом, обновлён до совместимости с цепочкой сертификатов
 * */
public interface CertAndKey {

    public X509Certificate[] getCertificateChain();

    public X509Certificate getCertificate();

    public KeyPair getKeyPair();

    public PrivateKey getPrivateKey();
}